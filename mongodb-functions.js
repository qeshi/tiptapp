db.tododb.insertOne({type:"add", text:"Diska"})

db.tododb.insertOne({type:"add", text:"Dammsug"})

db.tododb.insertOne({type:"add", text:"Bädda"})

db.tododb.insertOne({type:"remove", text:"Diska"})

db.tododb.insertOne({type:"markAsDone", text:"Dammsug"})
db.tododb.insertOne({type:"markAsUndone", text:"Dammsug"})

db.tododb.insertOne({type:"markAsDone", text:"Bädda"})

var eventMapper = function(){ emit(this.text, this) }

var eventReducer = function(key, values) {
  return values.sort(
    function(eventA, eventB){
      return eventA._id.getTimestamp() - eventB._id.getTimestamp();
    }).pop();
}

// { "_id" : "Diska", "value" : { "_id" : ObjectId("5a683d1bc2f5dec976194fc6"), "type" : "remove", "text" : "Diska" } }


var eventFinalizer = function(key, reducedEvent){

  if(reducedEvent.type === 'remove') {
    return null;
  } else {
    return {text: reducedEvent.text, marked: (reducedEvent.type === 'markAsDone')}
  }
}

// { "_id" : "Bädda", "value" : { "text" : "Bädda", "marked" : true } }
// { "_id" : "Diska", "value" : null }


db.tododb.mapReduce(eventMapper, eventReducer, {out:"mapReduceResult"} )

db.mapReduceResult.aggregate([ { $match : { value : { $ne : null }  }  },  {$replaceRoot: {newRoot: "$value"}}  ])

// { "text" : "Bädda", "marked" : true }
// { "text" : "Dammsug", "marked" : false }