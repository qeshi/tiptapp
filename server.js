'use strict';

const Hapi = require('hapi');
const Boom = require('boom');
const Joi = require('joi');

const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');

const databaseName = 'tododb'

const launchServer = async function() {

  const dbOpts = {
    url: `mongodb://localhost:27017/${databaseName}`,
    settings: {
      poolSize: 10
    },
    decorate: true
  };

  const swaggerOptions = {
    info: {
      title: 'TodoApp API Documentation',
      version: Pack.version,
    },
  };

  const server = Hapi.server({ port: 3001, host: '127.0.0.1' });

  await server.register([
    {
      plugin: require('hapi-mongodb'),
      options: dbOpts
    },
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: swaggerOptions
    }
  ]);


  server.route( {
    method: 'GET',
    path: '/todo-events/',
    async handler(request) {

      const db = request.mongo.db;

      try {
        const result = await db.collection(databaseName).find().sort( [['_id', -1]] ).toArray();
        return result;
      }
      catch (err) {
        throw Boom.internal('Internal MongoDB error', err);
      }
    },
    config: {
      description: 'Get all todo events',
      tags: ['api']
    }
  });

  server.route( {
    method: 'GET',
    path: '/todos/',
    async handler(request) {

      const db = request.mongo.db;

      var eventMapper = function(){ emit(this.text, this) }

      var eventReducer = function(key, values) {
        return values.sort(
          function(eventA, eventB){
            return eventA._id.getTimestamp() - eventB._id.getTimestamp();
          }).pop();
      }

      var eventFinalizer = function(key, reducedEvent){

        if(reducedEvent.type === 'remove') {
          return null;
        } else {
          return {text: reducedEvent.text, marked: (reducedEvent.type === 'markAsDone')}
        }
      }

      try {

        await db.collection(databaseName).mapReduce(eventMapper, eventReducer, {out:'mapReduceResult', finalize:eventFinalizer} )

        const cleanResult = await db.collection('mapReduceResult').aggregate([ { $match : { value : { $ne : null }  }  },  {$replaceRoot: {newRoot: "$value"}}  ]).toArray();

        return cleanResult;
      }
      catch (err) {
        throw Boom.internal('Internal MongoDB error', err);
      }
    },
    config: {
      tags: ['api'],
      description: 'Get all todo events',
      notes: 'Returns the current state from the todolist based on all events.'
    }
  });


  server.route( {
    method: 'POST',
    path: '/todos/{text}',
    async handler(request) {

      const db = request.mongo.db;
      const text = request.params.text

      try {
        const result = await db.collection(databaseName).insertOne({type:"add", text: text});
        return result;
      }
      catch (err) {
        throw Boom.internal('Internal MongoDB error', err);
      }
    },
    config: {
      tags: ['api'],
      description: 'Add todo',
      validate: {
        params: {
          text: Joi.string().min(1).max(100),
        }
      }
    }
  });

  server.route( {
    method: 'DELETE',
    path: '/todos/{text}',
    async handler(request) {

      const db = request.mongo.db;
      const text = request.params.text

      try {
        const result = await db.collection(databaseName).insertOne({type:"remove", text: text});
        return result;
      }
      catch (err) {
        throw Boom.internal('Internal MongoDB error', err);
      }
    },
    config: {
      tags: ['api'],
      description: 'Delete todo',
      notes: 'This operation does not delete anything from the database. It just adds a delete event.',
      validate: {
        params: {
          text: Joi.string().min(1).max(100)
        }
      }
    }
  });

  server.route( {
    method: 'PATCH',
    path: '/todos/{text}/mark-as-done',
    async handler(request) {

      const db = request.mongo.db;
      const text = request.params.text


      try {
        const result = await db.collection(databaseName).insertOne({type:"markAsDone", text: text});
        return result;
      }
      catch (err) {
        throw Boom.internal('Internal MongoDB error', err);
      }
    },
    config: {
      tags: ['api'],
      description: 'Mark todo as done',
      notes: 'This operation does not mutate the database state. It just adds a markAsDone event.',
      validate: {
        params: {
          text: Joi.string().min(1).max(100)
        }
      }
    }
  });

  server.route( {
    method: 'PATCH',
    path: '/todos/{text}/mark-as-undone',
    async handler(request) {

      const db = request.mongo.db;
      const text = request.params.text


      try {
        const result = await db.collection(databaseName).insertOne({type:"markAsUndone", text: text});
        return result;
      }
      catch (err) {
        throw Boom.internal('Internal MongoDB error', err);
      }
    },
    config: {
      tags: ['api'],
      description: 'Unmark todo as done',
      notes: 'This operation does not mutate the database state. It just adds a markAsUndone event.',
      validate: {
        params: {
          text: Joi.string().min(1).max(100),
        }
      }
    }
  });

  await server.start();
  console.log(`Server started at ${server.info.uri}`);
};

launchServer().catch((err) => {
  console.error(err);
  process.exit(1);
});