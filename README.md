## Getting started

* You need to start mongodb at localhost:27017 and add a database named "tododb"

* start server with ``` node server.js```

## Known bugs

* Swagger doesn't work for some odd reason. Maybe some problem with hapi v17??
https://github.com/glennjones/hapi-swagger/issues/472

* Dockerization still left to to...
* https://github.com/Automattic/mongoose/issues/5169


## Create an HTTP API that has the following functionality

* Add todos to a list,

* Remove todos from from the list,

* Mark todos as done,

* Unmark todos as done,

* Retrieve all events associated with a list of todos,

* The API should not loose any data if restarted.

## Things to think about

* The list of events, when fetched, should be presented in chronological order,

* An example of an event associated with a particular todo could be todoMarkedAsDone,

* Maybe think of how we can deploy this as a service, to AWS or similar?
